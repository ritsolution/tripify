import { TimelineLite, TweenLite } from 'gsap'

const tl = new TimelineLite()

export const closeHandler = () => {
  tl.to('#flight-header', 0.5, { marginTop: '-150%', display: 'none' })
    .to(
      '#header-arrow',
      0,
      {
        visibility: 'visible'
      },
      0
    )
    .to('#add-flight', 0, { display: 'none' }, 0)
    .to('#right-bar-overlay', 0, { display: 'none' }, 0)
    .to('#search-settings', 0, { display: 'block' }, 0)
}

export const directionsMapOpen = () => {
  TweenLite.to('#directionsMap2', 0.4, { right: 0 })
}

export const directionsMapClose = () => {
  TweenLite.to('#directionsMap2', 0.4, { right: '-800px' })
}
export const multipleMarkersMapClose = () => {
  TweenLite.to('#multipleMarkersMap2', 0.4, { left: '-800px' })
}


export const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list)
  const [removed] = result.splice(startIndex, 1)
  result.splice(endIndex, 0, removed)
  return result
}

export const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source)
  const destClone = Array.from(destination)
  const [removed] = sourceClone.splice(droppableSource.index, 1)

  destClone.splice(droppableDestination.index, 0, removed)

  const result = {}
  result[droppableSource.droppableId] = sourceClone
  result[droppableDestination.droppableId] = destClone

  return result
}
