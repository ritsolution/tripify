import React, { Component } from 'react'
import { MainWrapper, Background } from './styles'
import bg1 from '../../assets/images/bg1.jpg'
import bg2 from '../../assets/images/bg2.jpg'
import bg3 from '../../assets/images/bg5.jpg'
import bg4 from '../../assets/images/bg4.jpg'
import bg5 from '../../assets/images/bg7.jpg'
import {
  HomepageHeader,
  HomepageMiddle,
  HomepageFooter
  // LoginPopup
  // SignupPopup
} from '../../components'

class Home extends Component {
  state = { current: 0, images: [bg4, bg5, bg1, bg3, bg2] }

  componentDidMount() {
    this.interval = setInterval(() => {
      this.setState({ current: this.state.current + 1 })
      this.bgImageChanger()
    }, 10000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  bgImageChanger() {
    const { current, images } = this.state
    if (current === 4) {
      this.state.current = 0
      return <Background id="bgImage" bgImage={images[current]} />
    }
    return <Background id="bgImage" bgImage={images[current]} />
  }

  render() {
    const bigScreen = window.innerWidth > 600 && true

    return (
      <MainWrapper>
        {this.bgImageChanger()}
        <HomepageHeader />
        <HomepageMiddle />
        {bigScreen && <HomepageFooter />}
        {/* <LoginPopup /> */}
        {/* <SignupPopup /> */}
      </MainWrapper>
    )
  }
}

export default Home
