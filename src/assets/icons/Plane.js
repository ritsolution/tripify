import React from 'react'

const Plane = ({ width, height, color, styles }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 486.748 486.748"
    height={height}
    width={width}
    style={styles}
  >
    <path
      id="Plane"
      fill={color}
      d="M453.976,321.93v-24.184L282.944,178.793l3.891-111.207C284.874,9.286,247.374,0,241.903,0s-42.523,12.265-41.99,67.586
		l3.891,111.207L32.772,297.745v24.185l174.902-32.522l4.452,127.242l-63.868,44.42v10.923h69.429
		c5.004,8.5,14.084,14.368,24.644,14.737c11.339,0.397,21.419-5.592,26.783-14.737h69.375v-10.923l-63.867-44.42l4.451-127.242
		L453.976,321.93z"
    />
  </svg>
)
export default Plane
