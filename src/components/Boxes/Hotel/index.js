import React, { Component } from 'react'
import { TweenLite } from 'gsap'
import {
  MainWrapper,
  Img,
  Name,
  FirstColumn,
  LocationWrapper,
  MoreInfo,
  SecondColumn,
  PriceWrapper,
  Button,
  SectionWrapper,
  RoomsAndPrice,
  Rooms
} from './styles'
import Stars from '../../../assets/images/stars.png'
import { PinIcon, CloseX } from '../../../assets/icons'

class HotelBox extends Component {
  state = {}

  openHotelDetailsPopup = () => {
    TweenLite.to('#hotelDetailsPopup', 0.4, { display: 'block', opacity: 1 })
  }

  render() {
    const {
      imageURL,
      paddingLeft,
      noBorderRadius,
      name,
      closeButton,
      price,
      left,
      address,
      reviewsCount
    } = this.props
    return (
      <MainWrapper paddingLeft={paddingLeft}>
        <Img left={left} src={imageURL} noBorderRadius={noBorderRadius} />
        <FirstColumn>
          <Name>{name}</Name>
          <div style={{ display: 'flex', alignItems: 'baseline' }}>
            <img
              src={Stars}
              style={{ height: 12, width: 52, marginTop: 5, marginRight: 10, display: 'flex' }}
              alt="stars"
            />
            {reviewsCount} Reviews
          </div>
          <LocationWrapper>
            <PinIcon color="#484848" height={13} width={13} />
            {address}
          </LocationWrapper>
          <MoreInfo onClick={this.openHotelDetailsPopup}>More Details</MoreInfo>
        </FirstColumn>
        <SecondColumn>
          {closeButton && (
            <div style={{ right: 10, top: 15 }}>
              <CloseX color="rgb(51, 51, 51)" height={11} width={11} />
            </div>
          )}

          <SectionWrapper>
            <RoomsAndPrice>
              <PriceWrapper>${price}</PriceWrapper>
              <Rooms>2 rooms for 3 nights</Rooms>
            </RoomsAndPrice>
          </SectionWrapper>
          <Button>Book Now</Button>
        </SecondColumn>
      </MainWrapper>
    )
  }
}

export default HotelBox
