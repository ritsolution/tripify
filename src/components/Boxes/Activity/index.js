import React, { Component } from 'react'
import TweenLite from 'gsap'
import {
  MainWrapper,
  Img,
  Name,
  FirstColumn,
  LocationWrapper,
  MoreInfo,
  SecondColumn,
  PriceWrapper,
  SectionWrapper,
  RoomsAndPrice,
  Rooms,
  RatingWrapper,
  Time,
  Tags
} from './styles'
import Stars from '../../../assets/images/viatorstars.png'
import { PinIcon, CloseX } from '../../../assets/icons'

// eslint-disable-next-line react/prefer-stateless-function
class FlightBox extends Component {
  openAttractionDetailsPopup = () => {
    TweenLite.to('#attractionDetailsPopup', 0.4, { display: 'block', opacity: 1 })
  }

  render() {
    const {
      imageURL,
      paddingLeft,
      noBorderRadius,
      left,
      closed,
      listLength,
      closeButton,
      name,
      reviewsCount,
      price,
      address,
      duration,
      openHours
    } = this.props

    return (
      <MainWrapper paddingLeft={paddingLeft}>
        <Img left={left} src={imageURL} noBorderRadius={noBorderRadius} />
        <FirstColumn>
          <Name>{name}</Name>
          <RatingWrapper>
            <img
              src={Stars}
              style={{
                width: 75,
                display: 'flex',
                marginRight: 5
              }}
              alt="stars"
            />
            {reviewsCount} Reviews
          </RatingWrapper>
          <LocationWrapper>
            <PinIcon color="#484848" height={13} width={13} />
            {address}
          </LocationWrapper>
          {/* <Tags>Culture, Historic, Tradition, Nature, Beauty</Tags> */}
          <div style={{ marginTop: 10, display: 'flex', alignItems: 'center' }}>
            Open: {openHours}{' '}
            {closed &&
              listLength === 3 && (
                <div style={{ display: 'flex' }}>
                  <div style={{ marginLeft: 6 }}>| </div>
                  <div
                    style={{
                      backgroundColor: 'red',
                      borderRadius: 4,
                      color: 'white',
                      padding: '2px 5px',
                      marginLeft: 10
                    }}
                  >
                    Closed
                  </div>
                  <div style={{ marginLeft: 4, lineHeight: '20px' }}>at 09:10</div>
                </div>
              )}
          </div>
        </FirstColumn>
        <SecondColumn>
          {closeButton && (
            <div style={{ right: 10, top: 15 }}>
              <CloseX color="rgb(51, 51, 51)" height={11} width={11} />
            </div>
          )}
          <Time>{duration}</Time>
          <SectionWrapper>
            <RoomsAndPrice>
              <PriceWrapper>${price}</PriceWrapper>
              <Rooms>2 persons</Rooms>
            </RoomsAndPrice>
          </SectionWrapper>
          <MoreInfo onClick={this.openAttractionDetailsPopup}> More Details</MoreInfo>
        </SecondColumn>
      </MainWrapper>
    )
  }
}

export default FlightBox
