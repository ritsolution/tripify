import React, { Component } from 'react'
import { rotate } from 'gl-matrix/src/gl-matrix/mat2'
import {
  MainWrapper,
  FirstColumn,
  SecondColumn,
  PriceWrapper,
  Button,
  SectionWrapper,
  FirstRow,
  SecondRow,
  FlightInfo,
  DepartInfo,
  ArriveInfo,
  FlightTime,
  Stops,
  Time,
  Logo,
  Airlines
} from './styles'
import Gairways from '../../../assets/images/Gairways.png'
import pegasus from '../../../assets/images/pegasus.png'
import qatar from '../../../assets/images/qatar.png'
import airplane from '../../../assets/images/airplane.png'
import { PlaneIcon, CloseX } from '../../../assets/icons'

class FlightBox extends Component {
  state = {}

  render() {
    const {
      departureLogo,
      returnLogo,
      firstDepartureTime,
      firstArrivalTime,
      price,
      firstFlightTime,
      secondDepartureTime,
      secondArrivalTime,
      arriveIn,
      departureFrom,
      left
    } = this.props

    return (
      <MainWrapper>
        {left && (
          <div
            style={{
              position: 'absolute',
              marginLeft: 85,
              width: '85%',
              height: '100%',
              backgroundColor: 'white',
              display: 'flex',
              flexDirection: 'column',
              paddingTop: 20
            }}
          >
            <div style={{ right: '-9px', position: 'absolute', top: 9 }}>
              <CloseX color="rgb(51, 51, 51)" height={11} width={11} />
            </div>
            <div
              style={{
                right: 10,
                position: 'absolute',
                bottom: 25,
                display: 'flex',
                alignItems: 'center'
              }}
            >
              <div
                style={{
                  color: 'red',
                  fontWeight: 'bold',
                  fontSize: '20px'
                }}
              >
                $ 497
              </div>
              <div>/ 2 ways</div>
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <PlaneIcon color="rgb(51, 51, 51)" height={27} width={27} />
              <div style={{ fontWeight: 'bold', marginLeft: 15 }}>Depart From {departureFrom}</div>
            </div>
            <div
              style={{ marginLeft: 42, display: 'flex', alignItems: 'center', fontSize: '13px' }}
            >
              Duration: 2h 30min{' '}
              <div style={{ marginRight: 20, marginLeft: 20, fontSize: 13, color: 'green' }}>
                Nonstop
              </div>{' '}
              <div>
                {arriveIn === 'London (LND)' ? (
                  <img src={pegasus} style={{ height: 35 }} />
                ) : (
                  <img src={qatar} style={{ height: 35 }} />
                )}
              </div>
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <PlaneIcon
                color="rgb(51, 51, 51)"
                height={27}
                width={27}
                styles={{ transform: 'rotate(180deg)' }}
              />
              <div style={{ fontWeight: 'bold', marginLeft: 15 }}>Arrive in {arriveIn}</div>
            </div>
          </div>
        )}

        <Airlines>Luftansa + Georgian Airways</Airlines>
        <FirstColumn>
          <FirstRow>
            <Logo>
              <img alt="airways-icon" src={departureLogo} style={{ width: 70 }} />
            </Logo>
            <FlightInfo>
              <div>
                <div style={{  fontSize: '13px', fontWeight: 'bold' }}>Departure</div>

                <div style={{ display: 'flex' }}>
                  <DepartInfo>
                    <Time>{firstDepartureTime}</Time>
                    <div>LHR</div>
                  </DepartInfo>
                  <FlightTime>
                    {firstFlightTime}
                    <img alt="airways-icon" src={airplane} style={{ width: 105 }} />{' '}
                    <Stops>Non-stop</Stops>
                  </FlightTime>
                  <ArriveInfo>
                    <Time>{firstArrivalTime} </Time>
                    <div>LHR</div>
                  </ArriveInfo>
                </div>
              </div>
            </FlightInfo>
          </FirstRow>
          <SecondRow>
            <Logo>
              <img alt="airways-icon" src={returnLogo} style={{ width: 70 }} />
            </Logo>
            <FlightInfo>
              <div>
                <div style={{ fontWeight: 'bold',fontSize: '13px' }}>Return</div>
                <div style={{ display: 'flex' }}>
                  <DepartInfo>
                    <Time>{secondDepartureTime} </Time>
                    <div>ORD</div>
                  </DepartInfo>
                  <FlightTime>
                    8h 35m
                    <img alt="airways-icon" src={airplane} style={{ width: 105 }} />{' '}
                    <Stops>Non-stop</Stops>
                  </FlightTime>

                  <ArriveInfo>
                    <Time>{secondArrivalTime} </Time>
                    <div>ORD</div>
                  </ArriveInfo>
                </div>
              </div>
            </FlightInfo>
          </SecondRow>
        </FirstColumn>
        <SecondColumn>
          <SectionWrapper>
            <PriceWrapper>
              $ {price}
              <div style={{ fontWeight: 'normal', fontSize: '12' }}> / 2 ways</div>
            </PriceWrapper>
          </SectionWrapper>
          <Button>Buy Now</Button>
        </SecondColumn>
      </MainWrapper>
    )
  }
}

export default FlightBox
