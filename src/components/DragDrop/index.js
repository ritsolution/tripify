// import React, { Component } from 'react'
// import { Container, Draggable } from 'react-smooth-dnd'

// const groupStyle = {
//   marginLeft: '50px',
//   flex: 1
// }

// class Groups extends Component {
//   // eslint-disable-next-line
//   generateItems = (count, creator) => {
//     const result = []
//     for (let i = 0; i < count; i++) {
//       result.push(creator(i))
//     }
//     return result
//   }

//   state = {
//     items1: this.generateItems(10, i => ({ id: `1${i}`, data: `Draggable 1 - ${i}` })),
//     items2: this.generateItems(9, i => ({ id: `2${i}`, data: `Draggable 2 - ${i}` })),
//     items3: this.generateItems(10, i => ({ id: `3${i}`, data: `Draggable 3 - ${i}` })),
//     items4: this.generateItems(10, i => ({ id: `4${i}`, data: `Draggable 4 - ${i}` })),
//     items5: this.generateItems(10, i => ({ id: `5${i}`, data: `Draggable 4 - ${i}` })),
//     items6: this.generateItems(10, i => ({ id: `6${i}`, data: `Draggable 4 - ${i}` })),
//     items7: this.generateItems(10, i => ({ id: `7${i}`, data: `Draggable 4 - ${i}` })),
//     items8: this.generateItems(10, i => ({ id: `8${i}`, data: `Draggable 4 - ${i}` })),
//     items9: this.generateItems(10, i => ({ id: `9${i}`, data: `Draggable 4 - ${i}` })),
//     items10: this.generateItems(10, i => ({ id: `10${i}`, data: `Draggable 4 - ${i}` }))
//   }

//   applyDrag = (arr, dragResult) => {
//     const { removedIndex, addedIndex, payload } = dragResult
//     if (removedIndex === null && addedIndex === null) return arr
//     console.log(dragResult)
//     const result = [...arr]
//     let itemToAdd = payload

//     if (removedIndex !== null) {
//       itemToAdd = result.splice(removedIndex, 1)[0]
//     }

//     if (addedIndex !== null) {
//       result.splice(addedIndex, 0, itemToAdd)
//     }

//     return result
//   }

//   render() {
//     return (
//       <div
//         style={{
//           display: 'flex',
//           justifyContent: 'stretch',
//           marginTop: '50px',
//           marginRight: '50px'
//         }}
//       >
//         <div style={groupStyle}>
//           <Container
//             groupName="1"
//             getChildPayload={i => this.state.items1[i]}
//             onDrop={e => this.setState({ items1: this.applyDrag(this.state.items1, e) })}
//           >
//             {this.state.items1.map(p => (
//               <Draggable key={p.id}>
//                 <div className="draggable-item">{p.data}</div>
//               </Draggable>
//             ))}
//           </Container>
//         </div>
//         <div style={groupStyle}>
//           <Container
//             groupName="1"
//             getChildPayload={i => this.state.items2[i]}
//             onDrop={e => this.setState({ items2: this.applyDrag(this.state.items2, e) })}
//           >
//             {this.state.items2.map(p => (
//               <Draggable key={p.id}>
//                 <div className="draggable-item">{p.data}</div>
//               </Draggable>
//             ))}
//           </Container>
//         </div>
//         {/* <div style={groupStyle}>
//           <Container
//             groupName="1"
//             getChildPayload={i => this.state.items3[i]}
//             onDrop={e => this.setState({ items3: this.applyDrag(this.state.items3, e) })}
//           >
//             {this.state.items3.map(p => (
//               <Draggable key={p.id}>
//                 <div className="draggable-item">{p.data}</div>
//               </Draggable>
//             ))}
//           </Container>
//         </div>
//         <div style={groupStyle}>
//           <Container
//             groupName="1"
//             getChildPayload={i => this.state.items4[i]}
//             onDrop={e => this.setState({ items4: this.applyDrag(this.state.items4, e) })}
//           >
//             {this.state.items4.map(p => (
//               <Draggable key={p.id}>
//                 <div className="draggable-item">{p.data}</div>
//               </Draggable>
//             ))}
//           </Container>
//         </div>
//         <div style={groupStyle}>
//           <Container
//             groupName="1"
//             getChildPayload={i => this.state.items5[i]}
//             onDrop={e => this.setState({ items5: this.applyDrag(this.state.items5, e) })}
//           >
//             {this.state.items5.map(p => (
//               <Draggable key={p.id}>
//                 <div className="draggable-item">{p.data}</div>
//               </Draggable>
//             ))}
//           </Container>
//         </div>
//         <div style={groupStyle}>
//           <Container
//             groupName="1"
//             getChildPayload={i => this.state.items6[i]}
//             onDrop={e => this.setState({ items6: this.applyDrag(this.state.items6, e) })}
//           >
//             {this.state.items6.map(p => (
//               <Draggable key={p.id}>
//                 <div className="draggable-item">{p.data}</div>
//               </Draggable>
//             ))}
//           </Container>
//         </div>
//         <div style={groupStyle}>
//           <Container
//             groupName="1"
//             getChildPayload={i => this.state.items7[i]}
//             onDrop={e => this.setState({ items7: this.applyDrag(this.state.items7, e) })}
//           >
//             {this.state.items7.map(p => (
//               <Draggable key={p.id}>
//                 <div className="draggable-item">{p.data}</div>
//               </Draggable>
//             ))}
//           </Container>
//         </div>
//         <div style={groupStyle}>
//           <Container
//             groupName="1"
//             getChildPayload={i => this.state.items8[i]}
//             onDrop={e => this.setState({ items8: this.applyDrag(this.state.items8, e) })}
//           >
//             {this.state.items8.map(p => (
//               <Draggable key={p.id}>
//                 <div className="draggable-item">{p.data}</div>
//               </Draggable>
//             ))}
//           </Container>
//         </div>
//         <div style={groupStyle}>
//           <Container
//             groupName="1"
//             getChildPayload={i => this.state.items9[i]}
//             onDrop={e => this.setState({ items9: this.applyDrag(this.state.items9, e) })}
//           >
//             {this.state.items9.map(p => (
//               <Draggable key={p.id}>
//                 <div className="draggable-item">{p.data}</div>
//               </Draggable>
//             ))}
//           </Container>
//         </div>
//         <div style={groupStyle}>
//           <Container
//             groupName="1"
//             getChildPayload={i => this.state.items10[i]}
//             onDrop={e => this.setState({ items10: this.applyDrag(this.state.items10, e) })}
//           >
//             {this.state.items10.map(p => (
//               <Draggable key={p.id}>
//                 <div className="draggable-item">{p.data}</div>
//               </Draggable>
//             ))}
//           </Container>
//         </div> */}
//       </div>
//     )
//   }
// }

// Groups.propTypes = {}

// export default Groups

import React, { Component } from 'react'
import { Container, Draggable } from 'react-smooth-dnd'
import { GroupStyle, Wrapper } from './styles'
import externalState from '../../pages/TripPlanner/state'
import { FlightBox, AttractionBox, RestaurantBox, HotelBox } from '..'
import h2 from '../../assets/images/h2.jpg'
import h1 from '../../assets/images/h1.jpg'

class Groups extends Component {
  state = {
    APIdata: [
      { id: '123', data: 'Draggable 4 - 0' },
      { id: '124', data: 'Draggable 4 - 1' },
      { id: '125', data: 'Draggable 4 - 2' },
      { id: '123', data: 'Draggable 4 - 0' },
      { id: '124', data: 'Draggable 4 - 1' },
      { id: '125', data: 'Draggable 4 - 2' },
      { id: '123', data: 'Draggable 4 - 0' },
      { id: '124', data: 'Draggable 4 - 1' }
    ],
    days: [
      [
        { id: '223', data: 'Draggable 4 - 0' },
        { id: '224', data: 'Draggable 4 - 1' },
        { id: '225', data: 'Draggable 4 - 2' },
        { id: '223', data: 'Draggable 4 - 0' },
        { id: '224', data: 'Draggable 4 - 1' },
        { id: '225', data: 'Draggable 4 - 2' }
      ],
      [
        { id: '323', data: 'Draggable 4 - 0' },
        { id: '324', data: 'Draggable 4 - 1' },
        { id: '325', data: 'Draggable 4 - 2' },
        { id: '323', data: 'Draggable 4 - 0' },
        { id: '324', data: 'Draggable 4 - 1' }
      ],
      [
        { id: '423', data: 'Draggable 4 - 0' },
        { id: '424', data: 'Draggable 4 - 1' },
        { id: '425', data: 'Draggable 4 - 2' },
        { id: '423', data: 'Draggable 4 - 0' },
        { id: '424', data: 'Draggable 4 - 1' }
      ]
    ]
  }

  applyDrag = (arr, dragResult) => {
    const { removedIndex, addedIndex, payload } = dragResult
    if (removedIndex === null && addedIndex === null) return arr

    const result = [...arr]
    let itemToAdd = payload

    if (removedIndex !== null) {
      itemToAdd = result.splice(removedIndex, 1)[0]
    }

    if (addedIndex !== null) {
      result.splice(addedIndex, 0, itemToAdd)
    }

    return result
  }

  onDrop = (e, index) => {
    const { days } = this.state

    days[index] = this.applyDrag(days[index], e)
    this.setState({ days })
  }

  render() {
    const { APIdata, days } = this.state

    return (
      <Wrapper>
        <GroupStyle>
          <Container
            groupName="1"
            getChildPayload={i => APIdata[i]}
            onDrop={e => this.setState({ APIdata: this.applyDrag(APIdata, e) })}
          >
            {APIdata.map(p => (
              <Draggable key={p.id}>
                <div className="draggable-item">
                  <FlightBox imageURL={h1} />
                </div>
              </Draggable>
            ))}
          </Container>
        </GroupStyle>
        {days.map((day, index) => (
          <GroupStyle>
            <Container
              groupName="1"
              getChildPayload={i => day[i]}
              onDrop={e => this.onDrop(e, index)}
              // lockAxis="y"
            >
              {day.map(p => (
                <Draggable key={p.id}>
                  <div className="draggable-item">
                    <HotelBox imageURL={h2} />
                  </div>
                </Draggable>
              ))}
            </Container>
          </GroupStyle>
        ))}
      </Wrapper>
    )
  }
}

export default Groups
