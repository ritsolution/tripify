import React, { Component } from 'react'
import TweenLite from 'gsap'
import {
  Wrapper,
  Drop,
  InnerWrapper,
  MainWrapper,
  SelectedWrapper,
  Category,
  Stroke
} from './styles'
import {
  RestaurantIcon,
  AttractionIcon,
  ArrowDownIcon,
  AirplaneIcon,
  TransportIcon
} from '../../../assets/icons'
import { SkyscannerLogo, KayakLogo } from '../../../assets/logos'
import booking from '../../../assets/logos/booking.png'

class CustomDropdown extends Component {
  state = {
    dropOpen: false,
    selectedCategory: 'Skyscanner'
  }

  openHandler = () => {
    const { dropOpen } = this.state

    if (!dropOpen) {
      TweenLite.to(this.drop, 0.4, { height: 470 })
      TweenLite.to('#logo-dropdown-arrow', 0.25, { rotation: 180 })
      return this.setState({ dropOpen: true })
    }
    TweenLite.to(this.drop, 0.4, { height: 0 })
    TweenLite.to('#logo-dropdown-arrow', 0.25, { rotation: 0 })
    return this.setState({ dropOpen: false })
  }

  handleClickOutside = () => {
    TweenLite.to(this.drop, 0.4, { height: 0 })
    TweenLite.to('#logo-dropdown-arrow', 0.25, { rotation: 0 })
    this.setState({ dropOpen: false })
  }

  selectCategory = category => {
    this.setState({ selectedCategory: category, dropOpen: false })
    TweenLite.to(this.drop, 0.4, { height: 0 })
    TweenLite.to('#logo-dropdown-arrow', 0.25, { rotation: 0 })
  }

  renderIcon() {
    const { selectedCategory } = this.state

    switch (selectedCategory) {
      case 'Skyscanner':
        return <SkyscannerLogo color="#00b2d6" height={35} width={150} category="Flights" />
      case 'Kayak':
        return <KayakLogo color="#ec821f" height={30} width={200} />
      case 'Sightseeings':
        return <AttractionIcon color="#ec821f" height={20} width={20} />
      case 'Activities':
        return <TransportIcon color="#00b2d6" height={20} width={20} />
      case 'Restaurants':
        return <RestaurantIcon color="#00b2d6" height={20} width={20} />
      case 'Tours':
        return <AirplaneIcon color="#00b2d6" height={20} width={20} />
      case 'Transports':
        return <TransportIcon color="#00b2d6" height={20} width={20} />
      default:
        return null
    }
  }

  render() {
    const { openHandler, selectCategory, handleClickOutside } = this
    const { marginRightBottom, children, selected, header, width, height } = this.props

    return (
      <MainWrapper
        onBlur={handleClickOutside}
        tabIndex="0"
        marginRightBottom={marginRightBottom && true}
      >
        <Stroke>{header}</Stroke>
        <Wrapper onClick={openHandler}>
          <SelectedWrapper>
            <img alt="bnb" src={selected} style={{ width, height }} />
          </SelectedWrapper>
          <div id="logo-dropdown-arrow">
            <ArrowDownIcon color="#484848" height={12} width={12} />
          </div>
        </Wrapper>
        <Drop
          innerRef={comp => {
            this.drop = comp
          }}
        >
          <InnerWrapper>
            {children.map(child => (
              <Category key={child.props.src}>{child}</Category>
            ))}
          </InnerWrapper>
        </Drop>
      </MainWrapper>
    )
  }
}

export default CustomDropdown
