import React, { Component } from 'react'
import { Droppable, Draggable } from 'react-beautiful-dnd'
import { FlightBox, AttractionBox, RestaurantBox, HotelBox, ActivityBox } from '..'

const getItemStyle = (isDragging, draggableStyle) => ({
  userSelect: 'none',
  minHeight: 120,
  margin: `15px 0 ${8}px 0`,
  borderRadius: '4px',
  background: 'white',
  boxShadow: '0 1px 5px rgba(0, 0, 0, 0.2)',
  ...draggableStyle
})

const getListStyle = isDraggingOver => ({
  padding: '0 8px 20px 8px',
  maxWidth: 700,
  margin: 'auto'
})

class RightDrop extends Component {
  state = {}

  categoryItems = (
    price,
    img,
    departureLogo,
    returnLogo,
    firstDepartureTime,
    firstArrivalTime,
    secondDepartureTime,
    secondArrivalTime,
    firstFlightTime,
    name,
    arriveIn,
    departureFrom,
    address,
    nights,
    rooms,
    reviewsCount,
    people,
    duration,
    openHours,
    cost
  ) => {
    const { currentHeader } = this.props

    switch (currentHeader) {
      case 'Flights':
        return (
          <FlightBox
            price={price}
            firstDepartureTime={firstDepartureTime}
            departureLogo={departureLogo}
            returnLogo={returnLogo}
            secondDepartureTime={secondDepartureTime}
            secondArrivalTime={secondArrivalTime}
            firstArrivalTime={firstArrivalTime}
            firstFlightTime={firstFlightTime}
          />
        )
      case 'Hotels':
        return (
          <HotelBox
            imageURL={img}
            name={name}
            nights={nights}
            rooms={rooms}
            address={address}
            price={price}
            reviewsCount={reviewsCount}
          />
        )
      case 'Sightseeings':
        return (
          <AttractionBox
            imageURL={img}
            name={name}
            people={people}
            price={price}
            address={address}
            duration={duration}
            reviewsCount={reviewsCount}
            openHours={openHours}
          />
        )
      case 'Restaurants':
        return (
          <RestaurantBox
            imageURL={img}
            name={name}
            reviewsCount={reviewsCount}
            address={address}
            price={cost}
            people={people}
          />
        )
      case 'Activities':
        return (
          <ActivityBox
            imageURL={img}
            name={name}
            people={people}
            price={price}
            address={address}
            duration={duration}
            reviewsCount={reviewsCount}
            openHours={openHours}
          />
        )
      case 'Transports':
        return <HotelBox />
      default:
        return null
    }
  }

  render() {
    const { droppableId, listIndex, list } = this.props
    return (
      <Droppable key={`list-droppable-${listIndex}`} droppableId={droppableId}>
        {(provided2, snapshot2) => (
          <div ref={provided2.innerRef} style={getListStyle(snapshot2.isDraggingOver)}>
            {list &&
              list.map((Item, index) => (
                <Draggable key={Item.id} draggableId={Item.id} index={index}>
                  {(provided, snapshot) => (
                    <div>
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        style={getItemStyle(
                          snapshot.isDragging,
                          provided.draggableProps.style,
                          index
                        )}
                      >
                        {/* <Item.category imageURL={Item.img} noBorderRadius /> */}
                        {this.categoryItems(
                          Item.price,
                          Item.img,
                          Item.departureLogo,
                          Item.returnLogo,
                          Item.firstDepartureTime,
                          Item.firstArrivalTime,
                          Item.secondDepartureTime,
                          Item.secondArrivalTime,
                          Item.firstFlightTime,
                          Item.name,
                          Item.arriveIn,
                          Item.departureFrom,
                          Item.address,
                          Item.nights,
                          Item.rooms,
                          Item.reviewsCount,
                          Item.people,
                          Item.duration,
                          Item.openHours,
                          Item.cost
                        )}
                      </div>
                    </div>
                  )}
                </Draggable>
              ))}
            {provided2.placeholder}
          </div>
        )}
      </Droppable>
    )
  }
}

export default RightDrop
