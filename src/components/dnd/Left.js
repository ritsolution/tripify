import React, { Component } from 'react'
import { Droppable, Draggable } from 'react-beautiful-dnd'
import transport from '../../assets/images/transport.png'
import { FlightBox, AttractionBox, RestaurantBox, HotelBox } from '..'

import {
  Transport,
  Time,
  TimeWrapper,
  Edit,
  TransportationTime,
  DayEnd,
  Line,
  DayEndTime,
  MoonIconWrapper
} from './styles'
import { TaxiIcon, MoonIcon, PedestrianIcon, TrainIcon, BusIcon } from '../../assets/icons'
import day from '../../assets/images/day.png'
import transport2 from '../../assets/images/transportImg.png'
import timeEdit from '../../assets/images/timeEdit.png'
import { Button } from '../Headers/Hotel/styles'

const getItemStyle = (isDragging, draggableStyle, index) => ({
  userSelect: 'none',
  minHeight: 120,
  // margin: `70px 0 ${8}px 0`,
  marginTop: index === 0 ? '0' : '70px',
  borderRadius: '4px',
  background: 'white',
  boxShadow: '0 1px 5px rgba(0, 0, 0, 0.2)',
  ...draggableStyle
})

const getListStyle = isDraggingOver => ({
  padding: '70px 8px 20px 8px',
  maxWidth: 700,
  minHeight: '30vh',
  marginLeft: 'auto',
  position: 'relative'
})

class RightDrop extends Component {
  state = { editIsShowing: false }

  categoryItems = (
    category,
    price,
    img,
    departureLogo,
    returnLogo,
    firstDepartureTime,
    firstArrivalTime,
    secondDepartureTime,
    secondArrivalTime,
    firstFlightTime,
    name,
    arriveIn,
    departureFrom,
    address,
    nights,
    rooms,
    reviewsCount,
    people,
    duration,
    openHours,
    cost,
    list
  ) => {
    switch (category) {
      case 'Flights':
        return (
          <FlightBox
            imageURL={img}
            price={price}
            firstDepartureTime={firstDepartureTime}
            departureLogo={departureLogo}
            returnLogo={returnLogo}
            secondDepartureTime={secondDepartureTime}
            secondArrivalTime={secondArrivalTime}
            firstArrivalTime={firstArrivalTime}
            firstFlightTime={firstFlightTime}
            arriveIn={arriveIn}
            departureFrom={departureFrom}
            address={address}
            left
            closeButton
          />
        )
      case 'Hotels':
        return (
          <HotelBox
            imageURL={img}
            left
            name={name}
            closeButton
            nights={nights}
            rooms={rooms}
            address={address}
            price={price}
            reviewsCount={reviewsCount}
          />
        )
      case 'Sightseeings':
        return (
          <AttractionBox
            imageURL={img}
            closeButton
            left
            name={name}
            listLength={list.length}
            closed={img === '/static/media/at1.8cc7c9cf.jpg' && true}
            people={people}
            price={price}
            address={address}
            duration={duration}
            openHours={openHours}
          />
        )
      case 'Restaurants':
        return (
          <RestaurantBox
            closeButton
            imageURL={img}
            left
            name={name}
            reviewsCount={reviewsCount}
            address={address}
            price={cost}
            people={people}
          />
        )
      case 'Activities':
        return (
          <AttractionBox
            imageURL={img}
            closeButton
            left
            name={name}
            people={people}
            price={price}
            address={address}
            duration={duration}
            openHours={openHours}
          />
        )
      case 'Transports':
        return <HotelBox />
      default:
        return null
    }
  }

  render() {
    const { droppableId, listIndex, costs, list, date } = this.props
    const { editIsShowing } = this.state

    return (
      <Droppable key={`list-droppable-${listIndex}`} droppableId={droppableId}>
        {(provided, snapshot2) => (
          <div ref={provided.innerRef} style={getListStyle(snapshot2.isDraggingOver)}>
            <div
              style={{
                width: 120,
                height: 32,
                marginBottom: 45,
                position: 'absolute',
                boxShadow: '0 1px 5px rgba(0, 0, 0, 0.2)',
                top: '-15px',
                backgroundColor: '#505050',
                borderRadius: 30,
                color: 'white',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                fronWeight: 'bold',
                fontSize: '15px'
              }}
            >
              {date}
            </div>
            <div
              style={{
                maxWidth: 700,
                height: 120,
                border: '2px dashed silver',
                borderRadius: 4,
                position: 'absolute',
                width: '97.8%',
                display: list.length !== 0 ? 'none' : 'flex',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              Drag & Drop Flights / Hotels / Sightseeings / Restaurants
              <div
                style={{
                  height: 85,
                  position: 'absolute',
                  borderLeft: '2px dashed silver',
                  top: '-85px',
                  left: '40px',
                  zIndex: '-1',
                  display: list.length !== 0 ? 'none' : 'block'
                }}
              />
            </div>
            {list &&
              list.map((Item, index) => (
                <Draggable key={Item.id} draggableId={Item.id} index={index}>
                  {(provided, snapshot) => (
                    <div>
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        style={getItemStyle(
                          snapshot.isDragging,
                          provided.draggableProps.style,
                          index
                        )}
                      >
                        {Item.category ? (
                          <div style={{ position: 'relative' }}>
                            <div
                              style={{
                                height: 150,
                                position: 'absolute',
                                borderLeft: '2px dashed silver',
                                top: '-70px',
                                left: '40px',
                                zIndex: '-1',
                                display:
                                  snapshot2.isDraggingOver || snapshot.isDragging ? 'none' : 'block'
                              }}
                            />
                            <div style={{ position: 'absolute', top: '-50px', left: '80px' }}>
                              {index !== 0 && (
                                <div
                                  style={{
                                    display: snapshot.isDragging ? 'none' : 'flex',
                                    alignItems: 'center'
                                  }}
                                >
                                  <Item.icon width={28} height={28} color="gray" />
                                  <div style={{ position: 'relative' }}>
                                    <div
                                      style={{
                                        color: '#00b2d6',
                                        marginLeft: 15,
                                        marginRight: 15,
                                        cursor: 'pointer'
                                      }}
                                      onClick={() =>
                                        this.setState(prevState => ({
                                          showTransport: !prevState.showTransport
                                        }))
                                      }
                                    >
                                      Edit
                                    </div>
                                    <img
                                      src={transport2}
                                      alt="fds"
                                      style={{
                                        position: 'absolute',
                                        top: 20,
                                        left: 16,
                                        width: 265,
                                        boxShadow: '0 15px 20px 0 rgba(0,0,0,0.15)',
                                        zIndex: 5,
                                        display:
                                          this.state.showTransport && Item.id === 22
                                            ? 'block'
                                            : 'none'
                                      }}
                                    />
                                  </div>
                                  <div> {Item.transportDistance} /</div>
                                  <TransportationTime>{Item.transportTime} /</TransportationTime>
                                  <div style={{ color: 'red', marginLeft: 15 }}>
                                    {Item.transportCost}
                                  </div>
                                  <div
                                    style={{
                                      fontSize: 13,
                                      color: 'rgb(0, 178, 214)',
                                      cursor: 'pointer',
                                      marginLeft: 10
                                    }}
                                  >
                                    Details
                                  </div>
                                </div>
                              )}
                              {/* <TtransportEdit>Edit</TtransportEdit> */}
                              {/* <TransportationPrice>$ 100 </TransportationPrice> */}
                            </div>
                            <TimeWrapper>
                              <Time>{Item.startTime}</Time>
                              {!Item.date && (
                                <Edit onClick={() => this.setState({ editIsShowing: true })}>
                                  Edit
                                </Edit>
                              )}

                              <Time>{Item.finishTime}</Time>
                              {editIsShowing &&
                                Item.id === 22 && (
                                  <div
                                    style={{
                                      position: 'absolute',
                                      height: '70%',
                                      left: 12,
                                      backgroundColor: 'white',
                                      display: 'flex',
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      flexDirection: 'column'
                                    }}
                                  >
                                    {/* <img src={timeEdit} style={{ width: 54 }} /> */}
                                    <input
                                      style={{
                                        width: 63,
                                        height: 24,
                                        textAlign: 'center',
                                        borderRadius: '4px',
                                        boxShadow: '0 0 4px 1px rgba(0, 0, 0, 0.16)',
                                        border: 'none',
                                        outline: 'none'
                                      }}
                                      defaultValue="07 : 30"
                                    />
                                    <Button
                                      style={{
                                        boxShadow: 'none',
                                        width: 56,
                                        height: 22,
                                        margin: '5px 0'
                                      }}
                                      onClick={() => this.setState({ editIsShowing: false })}
                                    >
                                      Save
                                    </Button>
                                    {/* <img src={timeEdit} style={{ width: 54 }} /> */}
                                    <input
                                      style={{
                                        width: 63,
                                        height: 24,
                                        textAlign: 'center',
                                        boxShadow: '0 0 4px 1px rgba(0, 0, 0, 0.16)',
                                        border: 'none',
                                        borderRadius: '4px',
                                        outline: 'none'
                                      }}
                                      defaultValue="09 : 00"
                                    />
                                  </div>
                                )}
                            </TimeWrapper>
                            {/* <Item.category
                              imageURL={Item.img}
                              paddingLeft
                              noBorderRadius
                              price="2,432"
                            /> */}
                            {this.categoryItems(
                              Item.category,
                              Item.price,
                              Item.img,
                              Item.departureLogo,
                              Item.returnLogo,
                              Item.firstDepartureTime,
                              Item.firstArrivalTime,
                              Item.secondDepartureTime,
                              Item.secondArrivalTime,
                              Item.firstFlightTime,
                              Item.name,
                              Item.arriveIn,
                              Item.departureFrom,
                              Item.address,
                              Item.nights,
                              Item.rooms,
                              Item.reviewsCount,
                              Item.people,
                              Item.duration,
                              Item.openHours,
                              Item.cost,
                              list
                            )}
                          </div>
                        ) : (
                          <div style={{ textAlign: 'center', lineHeight: '110px' }}>
                            Drop Placeholder
                          </div>
                        )}
                      </div>

                      {list[list.length - 1] === Item &&
                        list.length === 4 &&
                        Item.category === 'Hotels' && (
                          <DayEnd style={{ display: snapshot2.isDraggingOver ? 'none' : 'flex' }}>
                            <DayEndTime>
                              <MoonIconWrapper>
                                <MoonIcon width={28} height={28} color="#00b2d6" />
                              </MoonIconWrapper>
                              Day ends at 8:25pm
                            </DayEndTime>
                            <Line />
                          </DayEnd>
                        )}
                    </div>
                  )}
                </Draggable>
              ))}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    )
  }
}

export default RightDrop
