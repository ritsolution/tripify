import styled from 'styled-components'

export const Transport = styled.div``

export const Time = styled.div``

export const TimeWrapper = styled.div`
  position: absolute;
  display: flex;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  background-color: white;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  width: 85px;
  padding: 15px 0;
  height: 100%;
  z-index: 2;
`

export const Edit = styled.div`
  color: #00b2d6;
  cursor: pointer;
`

export const TransportationTime = styled.div`
  padding-left: 12px;
`
export const TransportationDistance = styled.div`
  padding-left: 12px;
`
export const TransportationPrice = styled.div`
  padding-left: 12px;
  color: green;
`

export const TransportWrapper = styled.div`
  display: ${props => (props.isDragging ? 'none' : 'flex')};
  align-items: center;
`

export const TtransportEdit = styled.div`
  color: #00b2d6;
  padding-left: 20px;
`

export const DayEnd = styled.div`
  display: flex;
  justify-content: center;
  position: relative;
`

export const Line = styled.div`
  position: absolute;
  border-bottom: 0.7px solid silver;
  width: 95%;
  top: 18px;
`

export const DayEndTime = styled.div`
  z-index: 2;
  display: flex;
  align-items: center;
  padding: 0 10px;
  background-color: #f3f2f5;
`

export const MoonIconWrapper = styled.div`
  margin-right: 10px;
`
