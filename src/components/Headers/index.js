import FlightsHeader from './Flight'
import HotelsHeader from './Hotel'
import SightseeingsHeader from './Attraction'
import TransportsHeader from './Transport'
import RestaurantsHeader from './Restaurant'
import ActivitiesHeader from './Activity'
import RightFixedHeader from './RightFixedHeader'

export {
  FlightsHeader,
  HotelsHeader,
  SightseeingsHeader,
  TransportsHeader,
  RestaurantsHeader,
  ActivitiesHeader,
  RightFixedHeader
}
